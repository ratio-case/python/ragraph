[build-system]
requires = ["hatchling"]
build-backend = "hatchling.build"

[project]
name = "ragraph"
version = "1.22.7"
description = "Ratio graph handling in Python."
keywords = ["graph", "network", "analysis", "visualization"]
authors = [{ name = "Ratio Innovations B.V.", email = "info@ratio-case.nl" }]
license = "GPL-3.0-or-later"
license-files = ["LICENSE.md"]
readme = "README.md"
requires-python = ">=3.11"
dependencies = [
    "dd>=0.6.0",
    "lxml>=5.3.0",
    "numpy>=2.2.2",
    "ratio-genetic-py>=0.4.0",
    "ruamel-yaml>=0.18.10",
    "strenum>=0.4.15",
]

[project.optional-dependencies]
esl = ["raesl"]
plot = ["kaleido==0.2.1", "openchord", "plotly>=6.0.0"]
all = ["kaleido==0.2.1", "openchord", "plotly>=6.0.0", "raesl"]

[tool.uv]
package = true
default-groups = ["lint", "experiment", "test"]

[tool.uv.sources]
openchord = { git = "https://github.com/pke1029/open-chord.git" }

[dependency-groups]
lint = ["black>=25.1.0", "raver>=3.0.2", "ruff>=0.9.4"]
experiment = ["jupyterlab>=4.3.5", "pandas>=2.2.3"]
test = [
    "pytest>=8.3.4",
    "pytest-cov>=6.0.0",
    "pytest-examples>=0.0.15",
    "pytest-watcher>=0.4.3",
]

[project.urls]
Homepage = "https://ragraph.ratio-case.nl"
Repository = "https://gitlab.com/ratio-case-os/python/ragraph"
"Bug Tracker" = "https://gitlab.com/ratio-case-os/python/ragraph/-/issues"
Changelog = "https://ragraph.ratio-case.nl/CHANGELOG/"

[tool.black]
line-length = 100
target-version = ["py311"]

[tool.pytest.ini_options]
addopts = "-s --cov --cov-report xml:./.pytest_cache/coverage.xml --cov-report term-missing --basetemp ./tests/.temp"
testpaths = ["tests"]
markers = ["slow", "format"]

[tool.coverage.run]
source = ["ragraph"]
disable_warnings = ["module-not-imported"]

[tool.coverage.paths]
source = ["ragraph"]

[tool.coverage.report]
show_missing = true
skip_covered = true

[tool.raver]
ref = "origin/main"
module = "./src/ragraph/__init__.py"
changelog = "./docs/CHANGELOG.md"

[tool.ruff]
line-length = 100
lint.select = [
    "E", # pycodestyle
    "F", # pyflakes
]
target-version = "py311"
